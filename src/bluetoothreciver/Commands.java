package bluetoothreciver;


public class Commands {

    public static final String HELM="Helm Level Changed ";
    public static final String LEFT_TELEGRAPH="LEFT_TELEGRAPH Changed ";
    public static final String RIGHT_TELEGRAPH="RIGHT_TELEGRAPH Changed ";

    // Thruster Panel Commands
    public static final String T_BOW_RED_FULL="BowRedFull";
    public static final String T_BOW_RED_HALF="BowRedHALF";
    public static final String T_BOW_STOP="BowStop";
    public static final String T_BOW_GREEN_HALF="BowGreenHalf";
    public static final String T_BOW_GREEN_FULL="BowGreenFull";
    public static final String T_MOTOR_STOP="MotorStop";
    public static final String T_MOTOR_START="MotorStart";
    public static final String T_STERN_RED_FULL="SternRedFull";
    public static final String T_STERN_RED_HALF="SternRedHALF";
    public static final String T_STERN_STOP="SternStop";
    public static final String T_STERN_GREEN_HALF="SternGreenHalf";
    public static final String T_STERN_GREEN_FULL="SternGreenFull";

    // Winch Panel Commands
    public static final String W_SPEED_1="Speed 1";
    public static final String W_SPEED_2="Speed 2";
    public static final String W_SPEED_3="Speed 3";
    public static final String W_SPEED_4="Speed 4";
    public static final String W_EMERGE_RELEASE="EMERGE_RELEASE";
    public static final String W_BREAK="BREAK";
    public static final String W_AUTO="AUTO";
    public static final String W_MAN="MAN";
    public static final String W_CONSAT_RPM="CONSAT_RPM";
    public static final String W_COMB="COMB";
    public static final String W_STERV_VIEW="STERV_VIEW";
    public static final String W_WINCH_LEVEL="WINCH_LEVEL = ";

    // Camera Panel Commands
    public static final String C_LEFT_UP="LEFT_UP";
    public static final String C_LEFT="LEFT";
    public static final String C_LEFT_DOWN="LEFT_DOWN";
    public static final String C_UP="UP";
    public static final String C_DOWN="DOWN";
    public static final String C_RIGHT_UP="RIGHT_UP";
    public static final String C_RIGHT="RIGHT";
    public static final String C_RIGHT_DOWN="RIGHT_DOWN";
    public static final String C_PORT_WING="PORT_WING";
    public static final String C_STBD_WING="STBD_WING";
    public static final String C_BRDG="BRDG";
    public static final String C_PITCH="PITCH";
    public static final String C_RESET="RESET";







}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bluetoothreciver;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.bluetooth.UUID;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.StreamConnectionNotifier;
public class BluetoothServer {

    private boolean runState = true;

    public BluetoothServer(){

        try {UUID uuid = new UUID("04c6093b00001000800000805f9b34fb", false);
            StreamConnectionNotifier service = (StreamConnectionNotifier) Connector.open("btspp://localhost:" + uuid.toString() + ";name=SampleServer");
            System.out.println("open connection");

            while(runState == true){
                StreamConnection connection = (StreamConnection) service.acceptAndOpen();
                new Listener(connection).start();
            }

        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    private class Listener extends Thread{

        private OutputStream os;
        private InputStream is;

        public Listener(StreamConnection connection){
            try {
                this.os = connection.openOutputStream();
                os.flush();
                this.is = connection.openInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void run(){
            StringBuilder builder;
            try{
                String greeting = "JSR-82 RFCOMM server says hello";
                os.write(greeting.getBytes());

                while(true){
                    builder = new StringBuilder();
                    int tmp = 0;
                    while((tmp = is.read()) != -1){
                        builder.append((char) tmp);
                    }
                    System.out.println("received: " + builder.toString());
                }
            } 
            catch(IOException e){
                e.printStackTrace();
            }
        }
    }
}

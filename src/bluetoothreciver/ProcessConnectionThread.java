package bluetoothreciver;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.InputStream;

import javax.microedition.io.StreamConnection;



public class ProcessConnectionThread implements Runnable{

	private StreamConnection mConnection;
	
	// Constant that indicate command from devices
	private static final int EXIT_CMD = -1;

	
	public ProcessConnectionThread(StreamConnection connection)
	{
		mConnection = connection;
	}
	
	@Override
	public void run() {
		try {
			// prepare to receive data
			InputStream inputStream = mConnection.openInputStream();
			System.out.println("waiting for input");
	        
	        while (true) {
	        	int command = inputStream.read();
                    
	        	
	        	if (command == EXIT_CMD){	
	        		System.out.println("finish process");
	        		break;
	        	}
	        	
	        	processCommand(command);
//                        byte[] bytes = IOUtils.toByteArray(inputStream);
//                        String str=new String(bytes);
//                        System.out.println("\nCommand :: "+str);
                        
                      
                        
        	}
        } catch (Exception e) {
    		e.printStackTrace();
    	}
	}
	
	/**
	 * Process the command from client
	 * @param command the command code
	 */
	private void processCommand(int command) {
		try {
//			Robot robot = new Robot();
			switch (command) {
	    	case 1:
//	    		robot.keyPress(KeyEvent.VK_NUMPAD1);
	    		System.out.println(Commands.HELM);
	    		// release the key after it is pressed. Otherwise the event just keeps getting trigged	    		
//	    		robot.keyRelease(KeyEvent.VK_NUMPAD1);
	    		break;
	    	case 2:
	    		System.out.println(Commands.LEFT_TELEGRAPH);
	    		break;
                case 3:
	    		System.out.println(Commands.RIGHT_TELEGRAPH);
	    		break;
                case 4:
	    		System.out.println(Commands.T_BOW_RED_FULL);
	    		break;
                case 5:
	    		System.out.println(Commands.T_BOW_RED_HALF);
	    		break;
                case 6:
	    		System.out.println(Commands.T_BOW_STOP);
	    		break;
                case 7:
	    		System.out.println(Commands.T_BOW_GREEN_HALF);
	    		break;
                case 8:
	    		System.out.println(Commands.T_BOW_GREEN_FULL);
	    		break;
                case 9:
	    		System.out.println(Commands.T_MOTOR_STOP);
	    		break;
                case 10:
	    		System.out.println(Commands.T_MOTOR_START);
	    		break;
                case 11:
	    		System.out.println(Commands.T_STERN_RED_FULL);
	    		break;
                case 12:
	    		System.out.println(Commands.T_STERN_RED_HALF);
	    		break;
                case 13:
	    		System.out.println(Commands.T_STERN_STOP);
	    		break;
                case 14:
	    		System.out.println(Commands.T_STERN_GREEN_HALF);
	    		break;
                case 15:
	    		System.out.println(Commands.T_STERN_GREEN_FULL);
	    		break;
                case 16:
	    		System.out.println(Commands.W_SPEED_1);
	    		break;
                case 17:
	    		System.out.println(Commands.W_SPEED_2);
	    		break;
                case 18:
	    		System.out.println(Commands.W_SPEED_3);
	    		break;
                case 19:
	    		System.out.println(Commands.W_SPEED_4);
	    		break;
                case 20:
	    		System.out.println(Commands.W_EMERGE_RELEASE);
	    		break;
                case 21:
	    		System.out.println(Commands.W_BREAK);
	    		break;
                case 22:
	    		System.out.println(Commands.W_AUTO);
	    		break;
                case 23:
	    		System.out.println(Commands.W_MAN);
	    		break;
                case 24:
	    		System.out.println(Commands.W_CONSAT_RPM);
	    		break;
                case 25:
	    		System.out.println(Commands.W_COMB);
	    		break;
                case 26:
	    		System.out.println(Commands.W_STERV_VIEW);
	    		break;
                case 27:
	    		System.out.println(Commands.W_WINCH_LEVEL);
	    		break;
                case 28:
	    		System.out.println(Commands.C_LEFT_UP);
	    		break;
                case 29:
	    		System.out.println(Commands.C_LEFT);
	    		break;
                case 30:
	    		System.out.println(Commands.C_LEFT_DOWN);
	    		break;
                case 31:
	    		System.out.println(Commands.C_UP);
	    		break;
                case 32:
	    		System.out.println(Commands.C_DOWN);
	    		break;
                case 33:
	    		System.out.println(Commands.C_RIGHT_UP);
	    		break;
                case 34:
	    		System.out.println(Commands.C_RIGHT);
	    		break;
                case 35:
	    		System.out.println(Commands.C_RIGHT_DOWN);
	    		break;
                case 36:
	    		System.out.println(Commands.C_PORT_WING);
	    		break;
                case 37:
	    		System.out.println(Commands.C_STBD_WING);
	    		break;
                case 38:
	    		System.out.println(Commands.C_BRDG);
	    		break;
                case 39:
	    		System.out.println(Commands.C_PITCH);
	    		break;
                case 40:
	    		System.out.println(Commands.C_RESET);
	    		break;
                   
                    
			}
                        
                
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
